package info.mock.models;

import java.util.List;

/**
 * Model class for the book details.
 */
public class Ghost {

	private String name;
	private String destroyMethod;  
	private List<String> victims;

	public Ghost(String name, String destroyMethod, List<String> victims) {
		this.name = name;
		this.destroyMethod = destroyMethod;
		this.victims = victims;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDestroyMethod() {
		return destroyMethod;
	}
	public void setDestroyMethod(String destroyMethod) {
		this.destroyMethod = destroyMethod;
	}
	public List<String> getVictims() {
		return victims;
	}
	public void setVictims(List<String> victims) {
		this.victims = victims;
	}
}