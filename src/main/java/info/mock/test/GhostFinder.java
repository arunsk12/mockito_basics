package info.mock.test;

import info.mock.models.Ghost;

import java.util.Collections;
import java.util.List;

/**
* API layer for persisting and retrieving the Book objects.
*/
public class GhostFinder {

  private static GhostFinder bookDAL = new GhostFinder();

  public List<Ghost> getAllGhost(){
      return Collections.EMPTY_LIST;
  }

  public Ghost getGhost(String ghost){
      return null;
  }

  public static GhostFinder getInstance(){
      return bookDAL;
  }
}