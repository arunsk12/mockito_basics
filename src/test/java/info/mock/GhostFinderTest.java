package info.mock;

import info.mock.models.Ghost;
import info.mock.test.GhostFinder;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;


public class GhostFinderTest {

  private static GhostFinder mockedGhost;
  private static Ghost ghost1;
  private static Ghost ghost2;
  private static Ghost ghost3;
  private static Ghost ghost4;

  @BeforeClass
  public static void setUp(){
    mockedGhost = mock(GhostFinder.class);
    
    ghost1 = new Ghost("Vampire","Sucks blood", Arrays.asList("avis", "unhsiv", "amharb"));
    ghost2 = new Ghost("Zombie","Zombifies body", Arrays.asList("papa", "kiddie", "oak water"));   
    ghost3 = new Ghost("Casper","Kills you with love", Arrays.asList("Me", "You", "Others"));

    when(mockedGhost.getAllGhost()).thenReturn(Arrays.asList(ghost1, ghost2));
    
    when(mockedGhost.getGhost("Vampire")).thenReturn(ghost1);
    when(mockedGhost.getGhost("Zombie")).thenReturn(ghost2);
    when(mockedGhost.getGhost("Casper")).thenReturn(ghost3);
    
    when(mockedGhost.getGhost(null)).thenThrow(new NullPointerException());
  }

  @Test
  public void testGetAllGhosts() throws Exception {

    List<Ghost> ghostList = mockedGhost.getAllGhost();
    assertEquals(2, ghostList.size());
    
    Ghost _ghost = ghostList.get(0);
    assertNotNull(_ghost.getDestroyMethod());
    assertEquals("Vampire", _ghost.getName());
  }

  @Test
  public void testGetGhost(){
	
	assertNull(ghost4);  
	  
    String friendlyGhost = "Casper";
    Ghost casper = mockedGhost.getGhost(friendlyGhost);

    assertNotNull(casper);
    assertEquals(friendlyGhost, casper.getName());
    assertEquals("Kills you with love", casper.getDestroyMethod());

  }
}