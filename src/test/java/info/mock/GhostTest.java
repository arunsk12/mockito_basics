package info.mock;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for GhostTest.
 */
public class GhostTest extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public GhostTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( GhostTest.class );
    }


    public void testApp()
    {
        assertTrue( true );
    }
}
